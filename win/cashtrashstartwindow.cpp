#include "cashtrashstartwindow.h"
#include "ui_cashtrashstartwindow.h"

QString cashtrashStartWindow::settingsFileName_ = "cashtrash.ini";

void cashtrashStartWindow::loadSettings()
{
    settingsProcessor_.setFileName(settingsFileName_);
    settingsProcessor_.read();
    prevBaseFName_ = settingsProcessor_.getPrevBase();
    baseFolder_ = settingsProcessor_.getBaseFolder();
    
    if(!settingsProcessor_.getReadNorm()){
        ui->pbNew->setEnabled(false);
        ui->pbOpen->setEnabled(false);
        ui->pbPrev->setEnabled(false);
    }else{
        ui->pbPrev->setEnabled(QFile::exists(prevBaseFName_));
        ui->pbOpen->setEnabled(QDir(baseFolder_).exists());
        ui->pbNew->setEnabled(QDir(baseFolder_).exists());
    }    
}

cashtrashStartWindow::cashtrashStartWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::cashtrashStartWindow)
{
    settingsWindow_ = NULL;
    
    ui->setupUi(this);
    baseLoadWin_ = new CashTrash455m(this);
    
    connect(baseLoadWin_, SIGNAL(closeApp()),
            this, SLOT(actClose()));
    
    loadSettings();
    if(settingsProcessor_.getAutoload() && 
            QFile::exists(prevBaseFName_)){
        loadPrevBase();
    }
}

cashtrashStartWindow::~cashtrashStartWindow()
{
    delete ui;
}


void cashtrashStartWindow::on_pbClose_clicked()
{
    actClose();
}

void cashtrashStartWindow::on_pbOpen_clicked()
{
    if(baseLoadWin_ != NULL){

        QDir baseDir(baseFolder_);
        QStringList baseFileMask;
        baseFileMask << "*.xml";
        QStringList baseFilesFound = baseDir.entryList(baseFileMask, QDir::Files);
        
        if(baseFilesFound.size() == 0){
            QMessageBox::information(this, 
                                     "No bases", 
                                     QString("No bases found in folder [%1].")
                                        .arg(baseFolder_));
            return;
        }
        
        bool rez;
        QString currentBaseFilename = 
                QInputDialog::getItem(this, 
                                      "Select base to load", 
                                      QString("Current base directory\n[%1]")
                                        .arg(baseFolder_), 
                                      baseFilesFound, 
                                      0, 
                                      false, 
                                      &rez);
        if(!rez){
            return;
        }
        currentBaseFilename = baseFolder_ + currentBaseFilename;
        qDebug() << "curr base file" << currentBaseFilename;
        
        baseLoadWin_->setBaseFileName(currentBaseFilename);
        settingsProcessor_.setPrevBase(currentBaseFilename);
        settingsProcessor_.write();
        baseLoadWin_->show();
    }
}

void cashtrashStartWindow::actClose()
{
    settingsProcessor_.write();
    close();
}

void cashtrashStartWindow::updateBaseFolder(QString baseFolder)
{
    baseFolder_ = baseFolder;
    settingsProcessor_.setBaseFolder(baseFolder_);
    settingsProcessor_.write();
    loadSettings();
}

void cashtrashStartWindow::updateAutoload(bool checked)
{
    settingsProcessor_.setAutoload(checked);
    settingsProcessor_.write();
}

void cashtrashStartWindow::on_pbNew_clicked()
{
    QString newBaseFilename = 
            QInputDialog::getText(this, 
                                  "Base name", 
                                  "Please, input new base name");
    if(newBaseFilename.isEmpty()){
        return;
    }
    
    if(!newBaseFilename.contains("*.xml")){
        newBaseFilename += ".ctb.xml";
    }
    newBaseFilename = baseFolder_ + newBaseFilename;
    qDebug() << "new base filename: " << newBaseFilename;
    
    baseLoadWin_->setBaseFileName(newBaseFilename);
    settingsProcessor_.setPrevBase(newBaseFilename);
    settingsProcessor_.write();
    baseLoadWin_->show();
}

void cashtrashStartWindow::loadPrevBase()
{
    baseLoadWin_->setBaseFileName(prevBaseFName_);
    baseLoadWin_->show();    
}

void cashtrashStartWindow::on_pbPrev_clicked()
{
    loadPrevBase();
}

void cashtrashStartWindow::on_pbSettings_clicked()
{
    if(settingsWindow_ == NULL){
        settingsWindow_ = new SettingsWindow(this);
        
        connect(settingsWindow_, SIGNAL(baseFolderChanged(QString)),
                this, SLOT(updateBaseFolder(QString)));
        connect(settingsWindow_, SIGNAL(autoloadChanged(bool)),
                this, SLOT(updateAutoload(bool)));
    }
    
    settingsWindow_->setCurrentBaseFolder(baseFolder_);
    settingsWindow_->show();
}
