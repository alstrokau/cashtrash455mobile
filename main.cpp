#include "win/cashtrashstartwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qApp->setStyle(QStyleFactory::create("fusion"));
    cashtrashStartWindow w;
    w.show();
    
    return a.exec();
}
