#include "stattoppmtrans.h"
#include "algorithm"

StatTopPMTrans::StatTopPMTrans(){ }

QString StatTopPMTrans::toString() {
    QString rez = QString(">>>StatTopPMTrans [report]\n");
    for(int i = 0; i < topPTrans_.size(); i++){
        rez += topPTrans_.at(i).toString();
        rez += "\n";
    }
    rez += "---\n";
    for(int i = 0; i < topMTrans_.size(); i++){
        rez += topMTrans_.at(i).toString();
        rez += "\n";
    }
    
    return rez;
}

int comp(Transaction t1,Transaction t2){
    return t1.sum() > t2.sum();
}

void StatTopPMTrans::generate(const Database *database) {
    sortedTransactions_.clear();
    for(int i = 0; i < database->size(); i++){
        sortedTransactions_.push_back(database->item(i));
    }
    
    std::sort(sortedTransactions_.begin(), sortedTransactions_.end(), comp);
    
    topPTrans_.clear();
    for(int i = 0; i < sortedTransactions_.size(); i++){
        if(sortedTransactions_.at(i).sum() > 0){
			topPTrans_.push_back(sortedTransactions_.at(i));
        }
    }
    
    topMTrans_.clear();    
    for(int i = 0; i < sortedTransactions_.size(); i++){
        int index = sortedTransactions_.size() - 1 - i;
        if(sortedTransactions_.at(index).sum() < 0){
			topMTrans_.push_back(sortedTransactions_.at(index));
        }
    }
}

QString StatTopPMTrans::info() {
    return "StatTopPMTrans [info]";
}

TransactionList_t StatTopPMTrans::topPTrans() {
    return topPTrans_;
}

TransactionList_t StatTopPMTrans::topMTrans() {
    return topMTrans_;
}
