#ifndef TABLEWINDOW_H
#define TABLEWINDOW_H

#include <QMainWindow>
#include <ds/database.h>
#include "rr/tablerenderer.h"

enum RemoveButtonState{rbsOff, rbsClearBase, rbsRemoveItem};

namespace Ui {
class TableWindow;
}

class TableWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit TableWindow(QWidget *parent = 0, Database *database = NULL);
    ~TableWindow();
    
    void setDatabase(Database *database);
    
signals:
    void databaseChanged(Database database);
    
private slots:
    void on_pbAddTrans_clicked();
    void renderBase();
    void checkRemoveButton();
    
    void on_pbInitBalance_clicked();
    void on_pbClearBase_clicked();
    
private:
    Ui::TableWindow *ui;
    TableRenderer tr_;
    Database *db_;
    RemoveButtonState rbs;
};

#endif // TABLEWINDOW_H
