#include "chartsrenderer.h"
#include <QFont>
#include <QFontMetrics>
#include <cmath>

ChartsRenderer::ChartsRenderer(){}

void ChartsRenderer::setChartWidgets(QCustomPlot *catScat, QCustomPlot *SumOp){
    cpCatScat_ = catScat;
    cpSumOp_ = SumOp;
}

void ChartsRenderer::render(){}

void ChartsRenderer::renderCatScat(const Common::scatList_t subcats, const double catsum){
    if(cpCatScat_ == NULL){
        qDebug() << "catscat chart widget is null";
        return;
    }
    
    QList<QCPBars*> lb;
    qsrand(QTime::currentTime().msecsSinceStartOfDay());
    cpCatScat_->clearPlottables();
    
    for(int i = 0; i < subcats.size(); i++){
        QCPBars *barsBuff = new QCPBars(cpCatScat_->xAxis, cpCatScat_->yAxis);
        lb.push_back(barsBuff);
        cpCatScat_->addPlottable(barsBuff);
        QVector<double> key;
        key << 1.0;
        QVector<double> value;
        value << subcats.at(i).second;
        barsBuff->setData(key, value);
        barsBuff->setName(QString("[%2%]%1").arg(subcats.at(i).first).arg(QString::number(subcats.at(i).second / catsum * 100.0, 'f', 1)));
        QPen pen;
        pen.setWidthF(2.0);
        
        int r = 10 + qrand()%200;
        int g = 10 + qrand()%200;
        int b = 10 + qrand()%200;
        pen.setColor(QColor(r, g, b));
        barsBuff->setBrush(QColor(r, g, b, 60));
        barsBuff->setPen(pen);
        if(i > 0){
            if(subcats.at(i).second > 0){
                barsBuff->moveBelow(lb.at(i - 1));
            }else{
                barsBuff->moveAbove(lb.at(i - 1));
            }
        }
    }
    
    //cpCatScat_->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    cpCatScat_->xAxis->setRange(0.5, 1.5);
    cpCatScat_->xAxis->setVisible(false);
    
    if(catsum > 0){
        cpCatScat_->yAxis->setRange(0.0, catsum * 1.1);
    }else{
        cpCatScat_->yAxis->setRange(catsum * 1.1, 0.0);
    }
        
    cpCatScat_->legend->setVisible(true);
    cpCatScat_->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignVCenter|Qt::AlignRight);
    cpCatScat_->legend->setBrush(QColor(255, 255, 255, 200));
    QPen legendPen;
    legendPen.setColor(QColor(130, 130, 130, 200));
    cpCatScat_->legend->setBorderPen(legendPen);
    QFont legendFont = cpCatScat_->legend->font();
    legendFont.setPointSize(10);
    cpCatScat_->legend->setFont(legendFont);
    cpCatScat_->replot();
}

void ChartsRenderer::renderSumOp(){
    if(cpSumOp_ == NULL){
        qDebug() << "sumop chart widget is null";
        return;
    }
    
    cpSumOp_->clearGraphs();
    cpSumOp_->clearPlottables();
    cpSumOp_->clearItems();
    
    
    cpSumOp_->addGraph();
    QVector<double> dataDate;
    QVector<double> dataBalance;
    QVector<double> dataOp;
    QVector<QString> tickLabels;
    
    double maxBalance = 0.0;
    double minBalance = 0.0;
    double currentBalance = 0.0;
    int dateIndex = 0;
    
    dataBalance.push_back(db_.getInitBalance());
    dataDate.push_back(dateIndex);
    tickLabels.push_back("init");
    tickLabels.push_back("init");
    
    double maxOp = 0.0;
    double minOp = 0.0;
    for (int i = 0; i < db_.size(); ++i) {
        maxOp = std::max(maxOp, db_.item(i).sum());
        minOp = std::min(minOp, db_.item(i).sum());
        
        maxBalance = std::max(maxBalance, db_.item(i).balance());
        minBalance = std::min(minBalance, db_.item(i).balance());
    }
    
    QCPItemText *x = new QCPItemText(cpSumOp_);
    QFontMetrics fm(x->font());
    double xSWW = 1.0 * db_.size() * fm.height() / cpSumOp_->width();
    if(xSWW > floor(xSWW)){
        xSWW += 1.0;
    }
    int SWW = round(xSWW);
    
    if(SWW % 2 == 0){
        SWW++;
    }
    delete x;
    
    
    QString lastLabel = "init";
    for (int i = 0; i < db_.size(); ++i) {
        dataDate.push_back(++dateIndex);
        currentBalance = db_.item(i).balance();
        dataBalance.push_back(currentBalance);
        
        
        bool renderLabel = false;
        int minIndex = i - (SWW - 1)/2;
        int currIndex = minIndex;
        double max = 0.0;
        for(int j = 0; j < SWW; j++){
            if(currIndex >= 0 && currIndex < db_.size()){
                max = std::max(max, std::abs(db_.item(currIndex).sum()));
            }
            currIndex++;
        }
        if(max == std::abs(db_.item(i).sum())){
            renderLabel = true;
        }
        if(renderLabel){
            QCPItemText *textLabel = new QCPItemText(cpSumOp_);
            cpSumOp_->addItem(textLabel);
            textLabel->setPositionAlignment(Qt::AlignVCenter|Qt::AlignCenter);
            textLabel->position->setType(QCPItemPosition::ptPlotCoords);
            textLabel->position->setCoords(i, (maxBalance - minBalance)/2);
            textLabel->setText(QString("  %1.%2\t%3")
                               .arg(db_.item(i).cat())
                               .arg(db_.item(i).scat()) 
                               .arg(db_.item(i).sum()));
            textLabel->setRotation(-90.0);
            
            double factorAlpha;
            if(db_.item(i).sum() < 0.0){
                factorAlpha = db_.item(i).sum() / minOp;
            }else{
                factorAlpha = db_.item(i).sum() / maxOp;
            }
            textLabel->setColor(QColor(0,0,0,230 * factorAlpha + 20));
            textLabel->setVisible(renderLabel);
        }
        
        
        QString currLabel = db_.item(i).date().toString("dd.MM");
        if(lastLabel != currLabel){
            tickLabels.push_back(currLabel);
            lastLabel = currLabel;
        }else{
            tickLabels.push_back("");
        }
      
        dataOp.push_back(db_.item(i).sum());
    }
    
    
    QCPBars *opsBars = new QCPBars(cpSumOp_->xAxis, cpSumOp_->yAxis2);
    cpSumOp_->addPlottable(opsBars);
    opsBars->setData(dataDate, dataOp);
    opsBars->setPen(QPen(QColor::fromRgb(10, 70, 140, 100)));
    opsBars->setBrush(QColor(10, 70, 140, 50));
    cpSumOp_->yAxis2->setVisible(true);
    
    cpSumOp_->graph()->setData(dataDate, dataBalance);
    cpSumOp_->xAxis->setAutoTickLabels(false);
    cpSumOp_->xAxis->setTickVectorLabels(tickLabels);
    cpSumOp_->xAxis->setTickLabelRotation(70.0);
    
    QCPScatterStyle myScatter;
    myScatter.setShape(QCPScatterStyle::ssCircle);
    myScatter.setPen(QColor(QColor(100, 100, 100)));
    myScatter.setBrush(QColor(100, 100, 100, 40));
    myScatter.setSize(5);
    cpSumOp_->graph()->setScatterStyle(myScatter);
    
    cpSumOp_->xAxis->setAutoTickStep(false);
    cpSumOp_->xAxis->setTickStep(1.0);    
    cpSumOp_->xAxis->setRange(-0.1, dateIndex*1.1 );    
    
    QPen pen(QColor(100, 200, 100));
    pen.setWidthF(4.0);
    cpSumOp_->graph()->setPen(pen);
    cpSumOp_->graph()->setBrush(QBrush(QColor(100, 200, 100, 70)));
    cpSumOp_->rescaleAxes();
    cpSumOp_->replot();
    
    cpSumOp_->yAxis->setRange(minBalance, 1.1*maxBalance);
}
