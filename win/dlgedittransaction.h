#ifndef DLGEDITTRANSACTION_H
#define DLGEDITTRANSACTION_H

#include "ds/transaction.h"
#include "ds/database.h"
#include <QDialog>

namespace Ui {
class dlgEditTransaction;
}

class dlgEditTransaction : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgEditTransaction(QWidget *parent = 0, Database* database= NULL);
    ~dlgEditTransaction();
    Transaction& getTransaction();
    void setTransaction(const Transaction& transaction);
    
private slots:
    void getDataToTransaction();
    void fillCombos();
    
private:
    Ui::dlgEditTransaction *ui;
    Transaction tr_;
    Database* db_;
    
    void connectCombos();
    void disconnectCombos();
};

#endif // DLGEDITTRANSACTION_H
