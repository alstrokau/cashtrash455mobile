#ifndef CHARTWINDOW_H
#define CHARTWINDOW_H

#include <QMainWindow>
#include <ds/database.h>
#include "rr/chartsrenderer.h"


namespace Ui {
class ChartWindow;
}

class ChartWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ChartWindow(QWidget *parent = 0, const Database *database = NULL);
    ~ChartWindow();
    
    void setDatabase(Database *database);
    
private slots:
    void updateCatScat();
    
private:
    Ui::ChartWindow *ui;
    const Database *db_;
    QStringList qslProExp;
    
    ChartsRenderer chartRender_;
    
    void renderBase();
    void fillCatScatCombo();
    void connectCombo();
    void disconnectCombo();
};

#endif // CHARTWINDOW_H
