#include "dlgedittransaction.h"
#include "ui_dlgedittransaction.h"

dlgEditTransaction::dlgEditTransaction(QWidget *parent, Database* database) :
    QDialog(parent),
    ui(new Ui::dlgEditTransaction)
{
    ui->setupUi(this);
    ui->deDate->setDate(QDate::currentDate());
    connect(this, SIGNAL(accepted()), this, SLOT(getDataToTransaction())); 
    connectCombos();
            
    db_ = database;
    fillCombos();
}


dlgEditTransaction::~dlgEditTransaction()
{
    delete ui;
}

Transaction &dlgEditTransaction::getTransaction()
{
    return tr_;
}

void dlgEditTransaction::setTransaction(const Transaction &transaction)
{
    tr_ = transaction;
    
    ui->cbCat->setCurrentText(tr_.cat());
    ui->dsbSum->setValue(tr_.sum());
    ui->cbScat->setCurrentText(tr_.scat());
    ui->leComm->setText(tr_.comment());
    ui->deDate->setDate(tr_.date());
}

void dlgEditTransaction::getDataToTransaction()
{
    tr_.setData(ui->cbCat->currentText(), 
                ui->dsbSum->value(),
                ui->cbScat->currentText(), 
                ui->leComm->text(), 
                ui->deDate->date());
}

void dlgEditTransaction::fillCombos()
{
    if(db_ == NULL){
        return;
    }
    
    disconnectCombos();
    
    QString sBuff = ui->cbCat->currentText();
    ui->cbCat->clear();
    ui->cbCat->addItem("");
    ui->cbCat->addItems(db_->getCats());
    if(!sBuff.isEmpty()){
        ui->cbCat->setCurrentText(sBuff);
    }
    
    sBuff = ui->cbScat->currentText();
    ui->cbScat->clear();
    ui->cbScat->addItem("");
    ui->cbScat->addItems(db_->getScats(ui->cbCat->currentText()));
    if(!sBuff.isEmpty()){
        ui->cbScat->setCurrentText(sBuff);
    }
    
    connectCombos();
}

void dlgEditTransaction::connectCombos()
{
    connect(ui->cbCat, SIGNAL(currentTextChanged(QString)),
            this, SLOT(fillCombos()));
    connect(ui->cbScat, SIGNAL(currentTextChanged(QString)),
            this, SLOT(fillCombos()));
}

void dlgEditTransaction::disconnectCombos()
{
    disconnect(ui->cbCat, 0, 0, 0);
    disconnect(ui->cbScat, 0, 0, 0);
}
