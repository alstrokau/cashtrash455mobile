#include "cashtrash455m.h"
#include "ui_cashtrash455m.h"
#include <QDebug>

CashTrash455m::CashTrash455m(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CashTrash455m)
{
    tableWindow_ = NULL;
    chartsWindow_ = NULL;

    ui->setupUi(this);
    
    connect(ui->wHeader, SIGNAL(backPressed()),
            this, SLOT(actClose()));
}

CashTrash455m::~CashTrash455m()
{
    delete ui;
}

void CashTrash455m::setBaseFileName(const QString &filename)
{
    filename_ = filename;
    
    loadbase();
    QFileInfo fi(filename_);
    ui->wHeader->setTitle(fi.fileName());
    ui->wHeader->setBalance(db_.getCurrentBalance());
    renderBase();
}

void CashTrash455m::on_pbClose_clicked()
{
    saveBase();
    emit closeApp();
    close();
}

void CashTrash455m::loadbase()
{
    qDebug() << "attempt to load file " << filename_;
    db_ = ioXml::readFromXml(filename_);
    qDebug() << "db size:" << db_.size();
}

void CashTrash455m::renderBase()
{
    qDebug() << "attemt to render base";
    db_.recalculate();
}

void CashTrash455m::saveBase()
{
    ioXml::writeToXml(filename_, db_);
}

void CashTrash455m::actClose()
{
    saveBase();
    close();
}


void CashTrash455m::on_pbTable_clicked()
{
    if(tableWindow_ == NULL){
        tableWindow_ = new TableWindow(this, &db_);
        connect(tableWindow_, SIGNAL(databaseChanged(Database)),
                this, SLOT(updateDatabase(Database)));
    }
    
    tableWindow_->setDatabase(&db_);
    tableWindow_->show();
}

void CashTrash455m::on_pbCharts_clicked()
{
    if(chartsWindow_ == NULL){
        chartsWindow_ = new ChartWindow(this, &db_);
    }
    
    chartsWindow_->setDatabase(&db_);
    chartsWindow_->show();
}

void CashTrash455m::on_pbStatistics_clicked()
{
    if(statWindow_ == NULL){
        statWindow_ = new StatisticWindow(this, &db_);
    }
    
    statWindow_->setDatabase(&db_);
    statWindow_->show();
}

void CashTrash455m::updateDatabase(Database database)
{
    db_ = database;
    renderBase();
}

