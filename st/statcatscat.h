#ifndef STATCATSCAT_H
#define STATCATSCAT_H

#include "st/abstractstatmodule.h"
#include "ds/common.h"

//typedef QList<QPair<QString, double> > scatList_t;

class StatCatScat : public AbstractStatModule
{
public:
    StatCatScat();
    virtual void generate(const Database *database);
    virtual void generate(const Database *database, const QString category);
    virtual QString toString();
    virtual QString info();
    Common::scatList_t scatList();
    double catSum();
   
private:
    QList<QPair<QString, double> > scatList_;
    double catSum_;
    
    void clearData();
    static int comparator(const QPair<QString, double> a, const QPair<QString, double> b);
    void transformMapToList(QMap<QString, double> buffScatMap);
    QMap<QString, double> generateIntermediateMap(const QString category, const Database *database);
};

#endif // STATCATSCAT_H
