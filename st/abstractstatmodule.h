#ifndef ABSTRACTSTATMODULE_H
#define ABSTRACTSTATMODULE_H

#include "ds/database.h"
#include <QString>

class AbstractStatModule
{
public:
    AbstractStatModule();
    virtual void generate(const Database *database) = 0;
    virtual QString toString() = 0;
    virtual QString info();
    virtual ~AbstractStatModule();
    
protected:
//    const Database *db_;
};

#endif // ABSTRACTSTATMODULE_H
