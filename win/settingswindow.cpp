#include "settingswindow.h"
#include "ui_settingswindow.h"
#include <QInputDialog>
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>

SettingsWindow::SettingsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SettingsWindow)
{
    ui->setupUi(this);
    
    ui->wHeader->disableBalanceField();
    ui->wHeader->setTitle("SETTINGS");
    connect(ui->wHeader, SIGNAL(backPressed()),
            this, SLOT(close()));
}

SettingsWindow::~SettingsWindow()
{
    delete ui;
}

void SettingsWindow::setCurrentBaseFolder(const QString &baseFolder)
{
    baseFolder_ = baseFolder;
    ui->lBasesFolder->setText(baseFolder_);
}

void SettingsWindow::on_pbSetBasesFolder_clicked()
{
    QString folder;
#ifdef Q_OS_ANDROID
    folder = getAndroidFolder();
#endif
#ifdef Q_OS_WIN32
    folder = getDesktopFolder();
#endif
    
    if(!QDir(folder).exists()){
        QMessageBox::warning(this, "Path error", QString("Path\n[%1]\ndoes not exist. No changes performed.").arg(folder));
        return;
    }
    
    baseFolder_ = folder;
    updateBaseFolder(baseFolder_);
}

void SettingsWindow::on_pbDefault_clicked()
{
    QString defalutFolder;
    
#ifdef Q_OS_ANDROID
    defalutFolder = "/mnt/sdcard/";
#endif
#ifdef Q_OS_WIN32
    defalutFolder = "c:\\";
#endif

    baseFolder_ = defalutFolder;
    updateBaseFolder(baseFolder_);
}

void SettingsWindow::updateBaseFolder(const QString &baseFolder)
{
    ui->lBasesFolder->setText(baseFolder);
    emit baseFolderChanged(baseFolder);
}

QString SettingsWindow::getAndroidFolder()
{
    QString sBuff;
    sBuff = QInputDialog::getText(this, "Enter folder", "Please, type existing folder where bases live", QLineEdit::Normal, baseFolder_.isEmpty() ? "/mnt/sdcard/" : baseFolder_);
    
    checkDirTail(sBuff);
    return sBuff;
}

void SettingsWindow::checkDirTail(QString& sBuff)
{
    if(!sBuff.isEmpty() && sBuff[sBuff.length() - 1] != '/'){
        sBuff += "/";
    }    
}

QString SettingsWindow::getDesktopFolder()
{
    QString sBuff = QFileDialog::getExistingDirectory(this, "Select bases folder");
    qDebug() << "sbuff" << sBuff;
    
    checkDirTail(sBuff);
    return sBuff;
}

void SettingsWindow::on_chbAutoload_toggled(bool checked)
{
    emit autoloadChanged(checked);
}
