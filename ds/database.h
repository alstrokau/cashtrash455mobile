#ifndef DATABASE_H
#define DATABASE_H

#include <QDebug>
#include <QString>
#include <QList>

#include "ds/transaction.h"

typedef QList<Transaction> TransactionList_t;
enum FinanceType{ftAll, ftProfit, ftExpence};

class Database
{
public:
    Database();

    void addTransaction(const Transaction& transaction);
    int size() const;
    void removeTransaction(const uint index);
    void removeTransaction(const Transaction& transaction);
    int getTransactionIndex(const Transaction& transaction)const;
    void setTransactionByIndex(const Transaction& transaction, const int index);

    double getInitBalance() const;
    void setInitBalance(double value);

    double getCurrentBalance() const;

    void recalculate();
    QString baseInfo() const;
    Transaction item(const int index) const;    
    void clear();
    
    QStringList getCats(const FinanceType type = ftAll) const;
    QStringList getScats(const QString& category = "") const;

private:
    QList<Transaction> transactions_;
    double initBalance_;
    double currentBalance_;

    bool isIndexValid(const int index) const;
};

#endif // DATABASE_H
