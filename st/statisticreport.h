#ifndef STATISTICREPORT_H
#define STATISTICREPORT_H

#include "ds/database.h"
#include "st/statavpm.h"
#include "st/stattoppmtrans.h"
#include "st/stattoppmcats.h"
#include "st/statcatscat.h"

class StatisticReport
{
public:
    StatisticReport();
    void generate(const Database *database);
    QString toString();
    StatAvPM avPM;
    StatTopPMTrans topPMTrans;
    StatTopPMCats topPMCats;
    StatCatScat catScat;

private:
    const Database *database_;
};

#endif // STATISTICREPORT_H
