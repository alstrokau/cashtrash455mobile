#include "headerwidget.h"
#include "ui_headerwidget.h"

HeaderWidget::HeaderWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HeaderWidget)
{
    ui->setupUi(this);
    
    connect(ui->pbBack, SIGNAL(clicked()),
            this, SIGNAL(backPressed()));
}

HeaderWidget::~HeaderWidget()
{
    delete ui;
}

void HeaderWidget::setTitle(const QString &title)
{
    ui->lTitle->setText(title);
}

void HeaderWidget::setBalance(const double balance)
{
    ui->lBalance->setText(QString::number(balance));
}

void HeaderWidget::disableBalanceField()
{
    ui->lBalance->setVisible(false);
}
