#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QMainWindow>

namespace Ui {
class SettingsWindow;
}

class SettingsWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit SettingsWindow(QWidget *parent = 0);
    ~SettingsWindow();
    void setCurrentBaseFolder(const QString& baseFolder);
    
signals:
    void baseFolderChanged(const QString& newBaseFolder);
    void autoloadChanged(bool checked);
    
private slots:
    void on_pbSetBasesFolder_clicked();
    void on_pbDefault_clicked();
    void on_chbAutoload_toggled(bool checked);
    
private:
    Ui::SettingsWindow *ui;
    QString baseFolder_;
    
    void updateBaseFolder(const QString& baseFolder);
    QString getAndroidFolder();
    QString getDesktopFolder();
    void checkDirTail(QString &sBuff);
};

#endif // SETTINGSWINDOW_H
