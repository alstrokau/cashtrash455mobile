#ifndef STATTABLERENDERER_H
#define STATTABLERENDERER_H

#include "rr/abstractrenderer.h"
#include "st/statistic.h"
#include <QTableWidget>

class StatTableRenderer : public QObject, public AbstractRenderer
{
    Q_OBJECT
    
public:
    StatTableRenderer();
    void setTables(QTableWidget *tableAverage = NULL,
                   QTableWidget *tableTopTransPos = NULL,
                   QTableWidget *tableTopTransNeg = NULL,
                   QTableWidget *tableTopCatPos = NULL,
                   QTableWidget *tableTopCatNeg = NULL);
    void setStatistic(Statistic *statistic = NULL);
    
    void render();
    void renderCatScat(QTableWidget *tableWidget);    
    
private:
    QTableWidget *twAv_;
    QTableWidget *twTopTransPos_;
    QTableWidget *twTopTransNeg_;
    QTableWidget *twTopCatPos_;
    QTableWidget *twTopCatNeg_;
    Statistic *statistic_;
    
    void resizeTablesToContent();
    void renderAverage();
    void renderTopCatPos();
    void renderTopCatNeg();
    void renderTopTransPos();
    void renderTopTransNeg();
    
    void setupAverageTable();
    void setupTopCatPosTable();
    void setupTopCatNegTable();
    void setupTopOpPosTable();
    void setupTopOpNegTable();
    void setupCatScat(QTableWidget *tableWidget);
};

#endif // STATTABLERENDERER_H
