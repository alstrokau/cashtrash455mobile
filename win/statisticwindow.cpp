#include "statisticwindow.h"
#include "ui_statisticwindow.h"

static const QString CaptionBack = "<< Back to catigories";
static const QString CaptionToScats = "Watch subcatigories >>";

StatisticWindow::StatisticWindow(QWidget *parent, const Database *database) :
    QMainWindow(parent),
    ui(new Ui::StatisticWindow)
{
    ui->setupUi(this);
    

    
    db_ = database;
    
    connect(ui->wHeader, SIGNAL(backPressed()),
            this, SLOT(actClose()));
    
    stTableRenderer_.setStatistic(&stater_);
    stTableRenderer_.setTables(
                ui->twStatAverage, 
                ui->twStatTopOpPos, 
                ui->twStatTopOpNeg, 
                ui->twStatTopCatPos, 
                ui->twStatTopCatNeg);
    
    
    ui->wHeader->setTitle("STATISTIC");
    
    if(db_ != NULL){
        ui->wHeader->setBalance(db_->getCurrentBalance());
        renderBase();
    }
    
    ui->tabWidget->setCurrentIndex(0);
    
    connect(ui->twStatTopCatNeg, SIGNAL(cellClicked(int,int)),
            this, SLOT(processCellSelectionNeg(int, int)));
    connect(ui->twStatTopCatPos, SIGNAL(cellClicked(int,int)),
            this, SLOT(processCellSelectionPos(int, int)));
    
    ui->pbWatchCat->setText(CaptionToScats);
    tableModeNeg = tmCategory;
    ui->pbWatchCat->setVisible(false);
    ui->lCurrCat->setVisible(false);
    
    ui->pbWatchCatPos->setText(CaptionToScats);
    tableModePos = tmCategory;
    ui->pbWatchCatPos->setVisible(false);
    ui->lCurrCatPos->setVisible(false);
}

StatisticWindow::~StatisticWindow()
{
    delete ui;
}

void StatisticWindow::setDatabase(Database *database)
{
    db_ = database;
    renderBase();
}

void StatisticWindow::processCellSelectionNeg(int row, int)
{
    if(tableModeNeg == tmSubcategory){
        return;
    }
    
    if(ui->twStatTopCatNeg->selectedItems().count() != 0){
        selectedCategory_ = ui->twStatTopCatNeg->item(row, 0)->text();
        ui->pbWatchCat->setVisible(true);
    }else{
        ui->lCurrCat->setVisible(false);
        ui->pbWatchCat->setVisible(false);
    }
}

void StatisticWindow::processCellSelectionPos(int row, int)
{
    if(tableModePos == tmSubcategory){
        return;
    }
    
    if(ui->twStatTopCatPos->selectedItems().count() != 0){
        selectedCategory_ = ui->twStatTopCatPos->item(row, 0)->text();
        ui->pbWatchCatPos->setVisible(true);
    }else{
        ui->lCurrCatPos->setVisible(false);
        ui->pbWatchCatPos->setVisible(false);
    }
}

void StatisticWindow::actClose()
{
    ui->pbWatchCat->setVisible(false);
    ui->pbWatchCat->setText(CaptionToScats);
    ui->lCurrCat->setVisible(false);
    tableModeNeg = tmCategory;
    
    ui->pbWatchCatPos->setVisible(false);
    ui->pbWatchCatPos->setText(CaptionToScats);
    ui->lCurrCatPos->setVisible(false);
    tableModePos = tmCategory;
    
    close();
}

void StatisticWindow::renderBase()
{
    stater_.analyzeDatabase(db_);
    stTableRenderer_.render();
    ui->wHeader->setBalance(db_->getCurrentBalance());
}

void StatisticWindow::on_pbWatchCat_clicked()
{
    if(tableModeNeg == tmCategory){
        tableModeNeg = tmSubcategory;
        ui->pbWatchCat->setText(CaptionBack);
        ui->lCurrCat->setText(QString("Current category: %1").arg(selectedCategory_));
        ui->lCurrCat->setVisible(true);
        stater_.report.catScat.generate(db_, selectedCategory_);
        stTableRenderer_.renderCatScat(ui->twStatTopCatNeg);
    }else{
        tableModeNeg = tmCategory;
        ui->pbWatchCat->setVisible(false);
        ui->pbWatchCat->setText(CaptionToScats);
        ui->pbWatchCat->setVisible(false);
        ui->lCurrCat->setVisible(false);
        stTableRenderer_.render();
    }
}

void StatisticWindow::on_pbWatchCatPos_clicked()
{
    if(tableModePos == tmCategory){
        tableModePos = tmSubcategory;
        ui->pbWatchCatPos->setText(CaptionBack);
        ui->lCurrCatPos->setText(QString("Current category: %1").arg(selectedCategory_));
        ui->lCurrCatPos->setVisible(true);
        stater_.report.catScat.generate(db_, selectedCategory_);
        stTableRenderer_.renderCatScat(ui->twStatTopCatPos);
    }else{
        tableModePos = tmCategory;
        ui->pbWatchCatPos->setVisible(false);
        ui->pbWatchCatPos->setText(CaptionToScats);
        ui->pbWatchCatPos->setVisible(false);
        ui->lCurrCatPos->setVisible(false);
        stTableRenderer_.render();
    }
}
