#ifndef CASHTRASH455M_H
#define CASHTRASH455M_H

#include "ds/database.h"
#include "io/ioxml.h"

#include <QMainWindow>
#include "win/tablewindow.h"
#include "win/chartwindow.h"
#include "win/statisticwindow.h"
#include "widgets/headerwidget.h"


namespace Ui {
class CashTrash455m;
}

class CashTrash455m : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit CashTrash455m(QWidget *parent = 0);
    ~CashTrash455m();
    void setBaseFileName(const QString& filename);
    

private slots:
    void on_pbClose_clicked();
    void on_pbTable_clicked();
    void on_pbCharts_clicked();
    void on_pbStatistics_clicked();
    void updateDatabase(Database database);
    void actClose();

signals:
    void closeApp();
        
private:
    Ui::CashTrash455m *ui;
    Database db_;
    
    TableWindow *tableWindow_;
    ChartWindow *chartsWindow_;
    StatisticWindow *statWindow_;
    
    QString filename_;
            
    void loadbase();
    void renderBase();
    void saveBase();
};

#endif // CASHTRASH455M_H
