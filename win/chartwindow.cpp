#include "chartwindow.h"
#include "ui_chartwindow.h"
#include "st/statistic.h"

ChartWindow::ChartWindow(QWidget *parent, const Database *database) :
    QMainWindow(parent),
    ui(new Ui::ChartWindow)
{
    qslProExp << "[Expences]" << "[Profits]";
    
    ui->setupUi(this);
    
    db_ = database;
    
    connect(ui->wHeader, SIGNAL(backPressed()),
            this, SLOT(close()));
    
    chartRender_.setChartWidgets(ui->wCatScat, ui->wPlot);
    
    ui->wHeader->setTitle("CHARTS");
    if(db_ != NULL){
        ui->wHeader->setBalance(db_->getCurrentBalance());
        renderBase();
    }
    
    fillCatScatCombo();
}

ChartWindow::~ChartWindow()
{
    delete ui;
}

void ChartWindow::setDatabase(Database *database)
{
    db_ = database;
    renderBase();
}

void ChartWindow::renderBase()
{
    chartRender_.setDatabase(*db_);
    chartRender_.renderSumOp();
    ui->wHeader->setBalance(db_->getCurrentBalance());
}

void ChartWindow::fillCatScatCombo()
{
    disconnectCombo();
    
    ui->cbCatScat->clear();
    ui->cbCatScat->addItems(qslProExp);
    
    for (int i = 0; i < db_->getCats().size(); ++i) {
        ui->cbCatScat->addItem(db_->getCats().at(i));
    }
    
    connectCombo();
    updateCatScat();
}

void ChartWindow::connectCombo()
{
    connect(ui->cbCatScat, SIGNAL(currentIndexChanged(int)),
            this, SLOT(updateCatScat()));
}

void ChartWindow::disconnectCombo()
{
    connect(ui->cbCatScat, 0, 0, 0);;
}

void ChartWindow::updateCatScat()
{
    Common::scatList_t list;
    double catsum;
    Statistic stater;
    
    stater.analyzeDatabase(db_);
    
    if(qslProExp.contains(ui->cbCatScat->currentText())){
        if(ui->cbCatScat->currentText() == qslProExp.at(0)){
            list = stater.report.topPMCats.topMCats;
            catsum = stater.report.topPMCats.topMSum();
        }else{
            list = stater.report.topPMCats.topPCats;
            catsum = stater.report.topPMCats.topPSum();
        }
    }else{
        stater.report.catScat.generate(db_, ui->cbCatScat->currentText());
        
        list = stater.report.catScat.scatList();
        catsum = stater.report.catScat.catSum();
    }
    
    chartRender_.renderCatScat(list, catsum);
}
