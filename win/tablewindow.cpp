#include "tablewindow.h"
#include "ui_tablewindow.h"
#include "win/dlgedittransaction.h"
#include <QInputDialog>



TableWindow::TableWindow(QWidget *parent, Database *database) :
    QMainWindow(parent),
    ui(new Ui::TableWindow)
{
    ui->setupUi(this);
    
    connect(ui->wHeader, SIGNAL(backPressed()),
            this, SLOT(close()));
    connect(ui->twTransTable, SIGNAL(clicked(QModelIndex)),
            this, SLOT(checkRemoveButton()));
    
    db_ = database;
    
    ui->wHeader->setTitle("TABLE");
    if(db_ != NULL){
        ui->wHeader->setBalance(db_->getCurrentBalance());
    }
    
    tr_.setTableWidget(ui->twTransTable);
    tr_.setDatabase(*db_);
    renderBase();
    
    checkRemoveButton();
}

TableWindow::~TableWindow()
{
    delete ui;
}

void TableWindow::setDatabase(Database *database)
{
    db_ = database;
    renderBase();
}

void TableWindow::on_pbAddTrans_clicked()
{
    dlgEditTransaction* newTrans = new dlgEditTransaction(this, db_);
    if(newTrans->exec() == QDialog::Accepted){
        db_->addTransaction(newTrans->getTransaction());
        emit databaseChanged(*db_);
        renderBase();
    }
}

void TableWindow::renderBase()
{
    db_->recalculate();
    tr_.setDatabase(*db_);
    tr_.render();
    ui->wHeader->setBalance(db_->getCurrentBalance());
}

void TableWindow::checkRemoveButton()
{
    if(db_->size() == 0){
        rbs = rbsOff;
        ui->pbClearBase->setEnabled(false);
    }
    
    ui->pbClearBase->setEnabled(true);
    if(ui->twTransTable->selectedItems().count() != 0){
        rbs = rbsRemoveItem;
        ui->pbClearBase->setText("Remove item");
    }else{
        rbs = rbsClearBase;
        ui->pbClearBase->setText("Clear base");
    }
}

void TableWindow::on_pbInitBalance_clicked()
{
    double newInitBalance = QInputDialog::getDouble(
                this, 
                "Initial balance", 
                "Current init balance", 
                db_->getInitBalance());
    
    if(newInitBalance != db_->getInitBalance()){
        db_->setInitBalance(newInitBalance);
        emit databaseChanged(*db_);
        renderBase();
    }
}

void TableWindow::on_pbClearBase_clicked()
{
    if(rbs == rbsClearBase){
        db_->clear();
        emit databaseChanged(*db_);
        renderBase();
    }else if(rbs == rbsRemoveItem){
        int invertedIndex = db_->size() - ui->twTransTable->currentRow() - 1;
        db_->removeTransaction(db_->item(invertedIndex));
        emit databaseChanged(*db_);
        renderBase();
    }
}
