#include "statisticreport.h"

StatisticReport::StatisticReport()
{
}

void StatisticReport::generate(const Database *database)
{
    database_ = database;
    avPM.generate(database_);
    topPMTrans.generate(database_);
    topPMCats.generate(database_);
}

QString StatisticReport::toString()
{
    QString sReport;
    sReport += avPM.toString();
    sReport += "--------\n";
    sReport += topPMTrans.toString();
    sReport += "--------\n";
    sReport += topPMCats.toString();
    
    return sReport;
}
