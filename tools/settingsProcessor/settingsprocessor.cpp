#include "settingsprocessor.h"
#include <QDebug>

settingsProcessor::settingsProcessor()
{
}

void settingsProcessor::write()
{
    qDebug() << "attemt to write settings";
    
    if(fname_.isEmpty()){
        qDebug() << "settings filename is empty";
        return;
    }
    
    QSettings settings(fname_, QSettings::IniFormat);
    settings.setValue("readNorm", true);
    settings.setValue("baseFolder", baseFolder);
    settings.setValue("prevBase", prevBase);
    settings.setValue("autoload", autoload);
}

void settingsProcessor::read()
{
    qDebug() << "attemt to read settings";
    
    QSettings settings(fname_, QSettings::IniFormat);
    
    readNorm = settings.value("readNorm", false).toBool();
    baseFolder = settings.value("baseFolder", "").toString();
    prevBase = settings.value("prevBase", "").toString();
    autoload = settings.value("autoload", false).toBool();
    
    fname_ = settings.fileName();
}

QString settingsProcessor::fileName() const
{
    return fname_;
}

void settingsProcessor::setFileName(const QString &fname)
{
    fname_ = fname;
}
QString settingsProcessor::getBaseFolder() const
{
    return baseFolder;
}

void settingsProcessor::setBaseFolder(const QString &value)
{
    baseFolder = value;
}
QString settingsProcessor::getPrevBase() const
{
    return prevBase;
}

void settingsProcessor::setPrevBase(const QString &value)
{
    prevBase = value;
}
bool settingsProcessor::getReadNorm() const
{
    return readNorm;
}

void settingsProcessor::setReadNorm(bool value)
{
    readNorm = value;
}
bool settingsProcessor::getAutoload() const
{
    return autoload;
}

void settingsProcessor::setAutoload(bool value)
{
    autoload = value;
}





