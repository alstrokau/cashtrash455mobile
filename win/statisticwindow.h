#ifndef STATISTICWINDOW_H
#define STATISTICWINDOW_H

#include <QMainWindow>
#include "ds/database.h"
#include "st/statistic.h"
#include "rr/stattablerenderer.h"

enum TableMode{tmCategory, tmSubcategory};

namespace Ui {
class StatisticWindow;
}

class StatisticWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit StatisticWindow(QWidget *parent = 0, const Database *database = NULL);
    ~StatisticWindow();
    
    void setDatabase(Database *database);
    
private slots:
    void processCellSelectionNeg(int row, int);
    void processCellSelectionPos(int row, int);
    void actClose();
    
    void on_pbWatchCat_clicked();
    
    void on_pbWatchCatPos_clicked();
    
private:
    Ui::StatisticWindow *ui;
    const Database *db_;
    
    Statistic stater_;
    StatTableRenderer stTableRenderer_;
    QString selectedCategory_;
    
    TableMode tableModeNeg;
    TableMode tableModePos;
    
    void renderBase();
};

#endif // STATISTICWINDOW_H
