#-------------------------------------------------
#
# Project created by QtCreator 2014-03-07T14:32:32
#
#-------------------------------------------------

QT       += core gui printsupport xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cashtrash
TEMPLATE = app


SOURCES += main.cpp\
    win/cashtrash455m.cpp \
    rr/abstractrenderer.cpp \
    rr/chartsrenderer.cpp \
    rr/tablerenderer.cpp \
    rr/textrenderer.cpp \
    qcustomplot/qcustomplot.cpp \
    ds/common.cpp \
    ds/database.cpp \
    ds/transaction.cpp \
    io/ioxml.cpp \
    io/iodatabasestroage.cpp \
    win/dlgedittransaction.cpp \
    st/abstractstatmodule.cpp \
    st/statavpm.cpp \
    st/statcatscat.cpp \
    st/statistic.cpp \
    st/statisticreport.cpp \
    st/stattoppmcats.cpp \
    st/stattoppmtrans.cpp \
    rr/stattablerenderer.cpp \
    widgets/headerwidget.cpp \
    win/tablewindow.cpp \
    win/chartwindow.cpp \
    win/statisticwindow.cpp \
    tools/settingsProcessor/settingsprocessor.cpp \
    win/cashtrashstartwindow.cpp \
    win/settingswindow.cpp

HEADERS  += win/cashtrash455m.h \
    rr/abstractrenderer.h \
    rr/chartsrenderer.h \
    rr/tablerenderer.h \
    rr/textrenderer.h \
    qcustomplot/qcustomplot.h \
    ds/common.h \
    ds/database.h \
    ds/transaction.h \
    io/ioxml.h \
    io/iodatabasestroage.h \
    win/dlgedittransaction.h \
    st/abstractstatmodule.h \
    st/statavpm.h \
    st/statcatscat.h \
    st/statistic.h \
    st/statisticreport.h \
    st/stattoppmcats.h \
    st/stattoppmtrans.h \
    rr/stattablerenderer.h \
    widgets/headerwidget.h \
    win/tablewindow.h \
    win/chartwindow.h \
    win/statisticwindow.h \
    tools/settingsProcessor/settingsprocessor.h \
    win/cashtrashstartwindow.h \
    win/settingswindow.h

FORMS    += win/cashtrash455m.ui \
    win/dlgedittransaction.ui \
    widgets/headerwidget.ui \
    win/tablewindow.ui \
    win/chartwindow.ui \
    win/statisticwindow.ui \
    win/cashtrashstartwindow.ui \
    win/settingswindow.ui

CONFIG += mobility

INCLUDEPATH += \
    qcustomplot \
    widgets

MOBILITY = 

RESOURCES += \
    mobileRes.qrc

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

OTHER_FILES += \
    android/AndroidManifest.xml
