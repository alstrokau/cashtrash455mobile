#include "stattablerenderer.h"
#include <QHeaderView>
#include <QProgressBar>

StatTableRenderer::StatTableRenderer(){
    setTables();
    setStatistic();
}

void StatTableRenderer::setTables(QTableWidget *tableAverage, 
                                  QTableWidget *tableTopTransPos, 
                                  QTableWidget *tableTopTransNeg, 
                                  QTableWidget *tableTopCatPos, 
                                  QTableWidget *tableTopCatNeg){
    twAv_ = tableAverage;
    twTopTransPos_ = tableTopTransPos;
    twTopTransNeg_ = tableTopTransNeg;
    twTopCatPos_ = tableTopCatPos;
    twTopCatNeg_ = tableTopCatNeg;
//    twTopCatNeg_->horizontalHeader()->setStretchLastSection(true);
}

void StatTableRenderer::setStatistic(Statistic *statistic){
    statistic_ = statistic;
}

void StatTableRenderer::render(){
    if(statistic_ == NULL){
        return;
    }
    
    renderAverage();
    renderTopCatPos();
    renderTopCatNeg();
    renderTopTransPos();
    renderTopTransNeg();
    
    resizeTablesToContent();
}

void StatTableRenderer::renderAverage(){
    if(twAv_ == NULL){
        qDebug() << "twAv_ is NULL";
        return;
    }
    
    if(twAv_->rowCount() < 4 ||
            twAv_->columnCount() < 3){
        setupAverageTable();
    }
    
    QTableWidgetItem *twi;
    double valueP;
    double valueN;
    
    
    valueP = statistic_->report.avPM.avP();
    twi = new QTableWidgetItem(QString("%1").arg(valueP));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(0, 0, twi);
    valueN = statistic_->report.avPM.avM();
    twi = new QTableWidgetItem(QString("%1").arg(valueN));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(0, 1, twi);
    twi = new QTableWidgetItem(QString("%1").arg(valueN + valueP));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(0, 2, twi);
    
    valueP = statistic_->report.avPM.avPw();
    twi = new QTableWidgetItem(QString("%1").arg(valueP));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(1, 0, twi);
    valueN = statistic_->report.avPM.avMw();
    twi = new QTableWidgetItem(QString("%1").arg(valueN));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(1, 1, twi);
    twi = new QTableWidgetItem(QString("%1").arg(valueN + valueP));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(1, 2, twi);
    
    valueP = statistic_->report.avPM.avPm();
    twi = new QTableWidgetItem(QString("%1").arg(valueP));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(2, 0, twi);
    valueN = statistic_->report.avPM.avMm();
    twi = new QTableWidgetItem(QString("%1").arg(valueN));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(2, 1, twi);
    twi = new QTableWidgetItem(QString("%1").arg(valueN + valueP));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(2, 2, twi);
    
    valueP = statistic_->report.avPM.avPy();
    twi = new QTableWidgetItem(QString("%1").arg(valueP));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(3, 0, twi);
    valueN = statistic_->report.avPM.avMy();
    twi = new QTableWidgetItem(QString("%1").arg(valueN));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(3, 1, twi);
    twi = new QTableWidgetItem(QString("%1").arg(valueN + valueP));
    twi->setTextAlignment(Qt::AlignCenter);
    twAv_->setItem(3, 2, twi);
    
    twAv_->horizontalHeader()->setStretchLastSection(true);
    twAv_->resizeColumnsToContents();
    twAv_->resizeRowsToContents();
}

void StatTableRenderer::setupAverageTable()
{
    QStringList verticalLabels;
    QStringList horizontalLabels;
    
    verticalLabels << "D" << "W" << "M" << "Y";
    horizontalLabels << "Pos" << "Neg" << "Diff";
    
    twAv_->setRowCount(4);
    twAv_->setColumnCount(3);
    
    twAv_->setVerticalHeaderLabels(verticalLabels);
    twAv_->setHorizontalHeaderLabels(horizontalLabels);    
}


void StatTableRenderer::renderTopCatPos(){    
    if(twTopCatPos_ == NULL){
        qDebug() << "twTopCatPos_ is NULL";
        return;
    }
    
    setupTopCatPosTable();
    
    QTableWidgetItem *twi;
    QProgressBar *pbar;
    double value;
    QString sValue;
    
    twTopCatPos_->setRowCount(statistic_->report.topPMCats.topPCats.size());
    
    for(int i = 0; i < statistic_->report.topPMCats.topPCats.size(); i++){
        sValue = statistic_->report.topPMCats.topPCats.at(i).first;
        twi = new QTableWidgetItem(QString("%1").arg(sValue));
        twi->setTextAlignment(Qt::AlignVCenter | Qt::AlignRight);
        twTopCatPos_->setItem(i, 0, twi);
        
        value = statistic_->report.topPMCats.topPCats.at(i).second;
        twi = new QTableWidgetItem(QString("%1").arg(value));
        twi->setTextAlignment(Qt::AlignCenter);
        twTopCatPos_->setItem(i, 1, twi);
        
        pbar = new QProgressBar();
        pbar->setMinimum(0);
        pbar->setMaximum(100);
        pbar->setValue(100.0 * 
            statistic_->report.topPMCats.topPCats.at(i).second / 
            statistic_->report.topPMCats.topPSum());
        twTopCatPos_->setCellWidget(i, 2, pbar);
    }
    
    twTopCatPos_->horizontalHeader()->setStretchLastSection(true);
    twTopCatPos_->resizeColumnsToContents();
    twTopCatPos_->resizeRowsToContents();
}


void StatTableRenderer::setupTopCatPosTable()
{
    QStringList horizontalLabels;
    horizontalLabels << "Cat" << "Summa" << "Percent";
    
    twTopCatPos_->setColumnCount(3);
    twTopCatPos_->setHorizontalHeaderLabels(horizontalLabels); 
}


void StatTableRenderer::renderTopCatNeg(){
    if(twTopCatNeg_ == NULL){
        qDebug() << "twTopCatNeg_ is NULL";
        return;
    }
    
    setupTopCatNegTable();
    
    QTableWidgetItem *twi;
    QProgressBar *pbar;
    double value;
    QString sValue;
    
    twTopCatNeg_->setRowCount(statistic_->report.topPMCats.topMCats.size());
    
    for(int i = 0; i < statistic_->report.topPMCats.topMCats.size(); i++){
        sValue = statistic_->report.topPMCats.topMCats.at(i).first;
        twi = new QTableWidgetItem(QString("%1").arg(sValue));
        twi->setTextAlignment(Qt::AlignVCenter | Qt::AlignRight);
        twTopCatNeg_->setItem(i, 0, twi);
        
        value = statistic_->report.topPMCats.topMCats.at(i).second;
        twi = new QTableWidgetItem(QString("%1").arg(value));
        twi->setTextAlignment(Qt::AlignCenter);
        twTopCatNeg_->setItem(i, 1, twi);
        
        pbar = new QProgressBar();
        pbar->setMinimum(0);
        pbar->setMaximum(100);
        pbar->setValue(100.0 * 
            statistic_->report.topPMCats.topMCats.at(i).second / 
            statistic_->report.topPMCats.topMSum());
        twTopCatNeg_->setCellWidget(i, 2, pbar);
    }
    
    twTopCatNeg_->horizontalHeader()->setStretchLastSection(true);
    twTopCatNeg_->resizeColumnsToContents();
    twTopCatNeg_->resizeRowsToContents();
}

void StatTableRenderer::setupTopCatNegTable()
{
    QStringList horizontalLabels;
    horizontalLabels << "Cat" << "Summa" << "Percent";
    
    twTopCatNeg_->setColumnCount(3);
    twTopCatNeg_->setHorizontalHeaderLabels(horizontalLabels); 
}

void StatTableRenderer::renderTopTransPos(){
    if(twTopTransPos_ == NULL){
        qDebug() << "twTopTransPos_ is NULL";
        return;
    }
    
    if(twTopTransPos_->columnCount() < 4){
        setupTopOpPosTable();
    }
    
    QTableWidgetItem *twi;
    QProgressBar *pbar;
    double value;
    QString sValue;
    
    twTopTransPos_->setRowCount(statistic_->report.topPMTrans.topPTrans().size());
    
    for(int i = 0; i < statistic_->report.topPMTrans.topPTrans().size(); i++){
        sValue = statistic_->report.topPMTrans.topPTrans().at(i).cat();
        twi = new QTableWidgetItem(QString("%1").arg(sValue));
        twi->setTextAlignment(Qt::AlignVCenter | Qt::AlignRight);
        twTopTransPos_->setItem(i, 0, twi);
        
        sValue = statistic_->report.topPMTrans.topPTrans().at(i).scat();
        twi = new QTableWidgetItem(QString("%1").arg(sValue));
        twi->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        twTopTransPos_->setItem(i, 1, twi);
        
        value = statistic_->report.topPMTrans.topPTrans().at(i).sum();
        twi = new QTableWidgetItem(QString("%1").arg(value));
        twi->setTextAlignment(Qt::AlignCenter);
        twTopTransPos_->setItem(i, 2, twi);
        
//        sValue = statistic_->report.topPMTrans.topPTrans().at(i).date().toString("dd.MM.yyyy");
//        twi = new QTableWidgetItem(QString("%1").arg(sValue));
//        twi->setTextAlignment(Qt::AlignCenter);
//        twTopTransPos_->setItem(i, 3, twi);
        
//        sValue = statistic_->report.topPMTrans.topPTrans().at(i).comment();
//        twi = new QTableWidgetItem(QString("%1").arg(sValue));
//        twi->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
//        twTopTransPos_->setItem(i, 4, twi);
        
        pbar = new QProgressBar();
        pbar->setMinimum(0);
        pbar->setMaximum(100);
        pbar->setValue(100.0 * 
            statistic_->report.topPMTrans.topPTrans().at(i).sum() / 
            statistic_->report.topPMCats.topPSum());
        twTopTransPos_->setCellWidget(i, 3, pbar);
    }
    
    twTopTransPos_->horizontalHeader()->setStretchLastSection(true);
}

void StatTableRenderer::setupTopOpPosTable()
{
    QStringList horizontalLabels;
    horizontalLabels << "Cat" << "Scat" << "Sum" << "Percent";
    
    twTopTransPos_->setColumnCount(4);
    twTopTransPos_->setHorizontalHeaderLabels(horizontalLabels); 
}

void StatTableRenderer::renderTopTransNeg(){
    if(twTopTransNeg_ == NULL){
        qDebug() << "twTopTransNeg_ is NULL";
        return;
    }
    
    if(twTopTransNeg_->columnCount() < 4){
        setupTopOpNegTable();
    }
    
    QTableWidgetItem *twi;
    QProgressBar *pbar;
    double value;
    QString sValue;
    
    twTopTransNeg_->setRowCount(statistic_->report.topPMTrans.topMTrans().size());
    
    for(int i = 0; i < statistic_->report.topPMTrans.topMTrans().size(); i++){
        sValue = statistic_->report.topPMTrans.topMTrans().at(i).cat();
        twi = new QTableWidgetItem(QString("%1").arg(sValue));
        twi->setTextAlignment(Qt::AlignVCenter | Qt::AlignRight);
        twTopTransNeg_->setItem(i, 0, twi);
        
        sValue = statistic_->report.topPMTrans.topMTrans().at(i).scat();
        twi = new QTableWidgetItem(QString("%1").arg(sValue));
        twi->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        twTopTransNeg_->setItem(i, 1, twi);
        
        value = statistic_->report.topPMTrans.topMTrans().at(i).sum();
        twi = new QTableWidgetItem(QString("%1").arg(value));
        twi->setTextAlignment(Qt::AlignCenter);
        twTopTransNeg_->setItem(i, 2, twi);
        
//        sValue = statistic_->report.topPMTrans.topMTrans().at(i).date().toString("dd.MM.yyyy");
//        twi = new QTableWidgetItem(QString("%1").arg(sValue));
//        twi->setTextAlignment(Qt::AlignCenter);
//        twTopTransNeg_->setItem(i, 3, twi);
        
//        sValue = statistic_->report.topPMTrans.topMTrans().at(i).comment();
//        twi = new QTableWidgetItem(QString("%1").arg(sValue));
//        twi->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
//        twTopTransNeg_->setItem(i, 4, twi);
        
        pbar = new QProgressBar();
        pbar->setMinimum(0);
        pbar->setMaximum(100);
        pbar->setValue(100.0 * 
            statistic_->report.topPMTrans.topMTrans().at(i).sum() / 
            statistic_->report.topPMCats.topMSum());
        twTopTransNeg_->setCellWidget(i, 3, pbar);
    }
    
    twTopTransNeg_->horizontalHeader()->setStretchLastSection(true);
}

void StatTableRenderer::renderCatScat(QTableWidget *tableWidget)
{
    setupCatScat(tableWidget);
    
    QTableWidgetItem *twi;
    QProgressBar *pbar;
    double value;
    QString sValue;
    
    tableWidget->setRowCount(statistic_->report.catScat.scatList().size());
    
    
    int index;
    for(int i = 0; i < statistic_->report.catScat.scatList().size(); i++){
        if(statistic_->report.catScat.catSum() > 0.0){
            index = i;
        }else{
            index = statistic_->report.catScat.scatList().size() - i - 1;
        }
            
        sValue = statistic_->report.catScat.scatList().at(index).first;
        twi = new QTableWidgetItem(QString("%1").arg(sValue));
        twi->setTextAlignment(Qt::AlignVCenter | Qt::AlignRight);
        tableWidget->setItem(i, 0, twi);
        
        value = statistic_->report.catScat.scatList().at(index).second;
        twi = new QTableWidgetItem(QString("%1").arg(value));
        twi->setTextAlignment(Qt::AlignCenter);
        tableWidget->setItem(i, 1, twi);
        
        pbar = new QProgressBar();
        pbar->setMinimum(0);
        pbar->setMaximum(100);
        pbar->setValue(100.0 * 
            statistic_->report.catScat.scatList().at(index).second / 
            statistic_->report.catScat.catSum());
        tableWidget->setCellWidget(i, 2, pbar);
    }
    
    tableWidget->horizontalHeader()->setStretchLastSection(true);
    tableWidget->resizeColumnsToContents();
    tableWidget->resizeRowsToContents();
}

void StatTableRenderer::setupCatScat(QTableWidget *tableWidget)
{
    QStringList horizontalLabels;
    horizontalLabels << "Scat" << "Summa" << "Percent";
    
    tableWidget->setColumnCount(3);
    tableWidget->setHorizontalHeaderLabels(horizontalLabels); 
}

void StatTableRenderer::setupTopOpNegTable()
{
    QStringList horizontalLabels;
    horizontalLabels << "Cat" << "Scat" << "Sum" << "Percent";
    
    twTopTransNeg_->setColumnCount(4);
    twTopTransNeg_->setHorizontalHeaderLabels(horizontalLabels); 
}


void StatTableRenderer::resizeTablesToContent(){
    if(twAv_){
        twAv_->resizeRowsToContents();
        twAv_->resizeColumnsToContents();
    }
    
    if(twTopTransPos_){
        twTopTransPos_->resizeRowsToContents();
        twTopTransPos_->resizeColumnsToContents();
    }
    
    if(twTopTransNeg_){
        twTopTransNeg_->resizeRowsToContents();
        twTopTransNeg_->resizeColumnsToContents();
    }
    
    if(twTopCatPos_){
        twTopCatPos_->resizeRowsToContents();
        twTopCatPos_->resizeColumnsToContents();
    }
    
    if(twTopCatNeg_){
        twTopCatNeg_->resizeRowsToContents();
        twTopCatNeg_->resizeColumnsToContents();
    }
}
