#include "tablerenderer.h"
#include <QScrollBar>
#include <QHeaderView>

TableRenderer::TableRenderer() :
    tableWidget_(NULL)
{}

TableRenderer::TableRenderer(QTableWidget *tableWidget) :
    tableWidget_(tableWidget)
{
    
}

void TableRenderer::setTableWidget(QTableWidget *tableWidget)
{
    tableWidget_ = tableWidget;
}

void TableRenderer::render()
{
    generateColorTable();
    tableWidget_->setRowCount(db_.size());
    tableWidget_->setSelectionBehavior(QAbstractItemView::SelectRows);
    
    int row = 0;

    for(int i = db_.size() - 1; i >= 0; i--){
        QTableWidgetItem *wi;

        wi = new QTableWidgetItem(db_.item(i).cat());
        wi->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        wi->setBackgroundColor(colorTable_.at(db_.getCats().indexOf(db_.item(i).cat())));
        tableWidget_->setItem(row, 0, wi);
        
        wi = new QTableWidgetItem(db_.item(i).scat());
        wi->setBackgroundColor(colorTable_.at(db_.getCats().indexOf(db_.item(i).cat())).lighter());
        tableWidget_->setItem(row, 1, wi);
        
        wi = new QTableWidgetItem(QString::number(db_.item(i).sum()));
        wi->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        if(db_.item(i).sum() > 0){
            wi->setBackgroundColor(qRgba(220, 255, 220, 50));
        }else{
            wi->setBackgroundColor(qRgba(255, 220, 220, 50));
        }        
        tableWidget_->setItem(row, 2, wi);
        
        wi = new QTableWidgetItem(db_.item(i).date().toString("dd.MM.yyyy"));
        wi->setTextAlignment(Qt::AlignCenter);
        tableWidget_->setItem(row, 3, wi);

//        wi = new QTableWidgetItem(db_.item(i).comment());
//        if(db_.item(i).comment().contains("[plan]")){
//            wi->setBackgroundColor(QColor(200, 200, 200));
//        }
//        tableWidget_->setItem(row, 4, wi);
        
//        wi = new QTableWidgetItem(QString::number(db_.item(i).balance()));
//        wi->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
//        tableWidget_->setItem(row, 5, wi);

        row++;
    }
    
    tableWidget_->resizeColumnsToContents();
    tableWidget_->resizeRowsToContents();
    tableWidget_->setTextElideMode(Qt::ElideRight);
    tableWidget_->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
}

void TableRenderer::setupTableWidget()
{
    tableWidget_->setRowCount(1);
}

void TableRenderer::generateColorTable()
{
    if(db_.size() < 1){
        return;
    }
    
    qsrand(QTime::currentTime().msecsSinceStartOfDay());
    
    colorTable_.clear();
    for(int i = 0; i < db_.size(); i++){
        colorTable_.push_back(
                    QColor::fromHsv(qrand()%190 + 70, 
                                    qrand()%150 + 100, 
                                    qrand()%150 + 100, 
                                    60));
    }
}
