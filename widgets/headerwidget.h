#ifndef HEADERWIDGET_H
#define HEADERWIDGET_H

#include <QWidget>

namespace Ui {
class HeaderWidget;
}

class HeaderWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit HeaderWidget(QWidget *parent = 0);
    ~HeaderWidget();
    
    void setTitle(const QString& title);
    void setBalance(const double balance);
    void disableBalanceField();
    
signals:
    void backPressed();
    
private:
    Ui::HeaderWidget *ui;
};

#endif // HEADERWIDGET_H
