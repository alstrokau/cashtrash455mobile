#ifndef SETTINGSPROCESSOR_H
#define SETTINGSPROCESSOR_H

#include <QSettings>

class settingsProcessor
{
public:
    settingsProcessor();
    void write();
    void read();
    
    QString fileName() const;
    void setFileName(const QString &fileName);
    
    QString getBaseFolder() const;
    void setBaseFolder(const QString &value);
    
    QString getPrevBase() const;
    void setPrevBase(const QString &value);
    
    bool getReadNorm() const;
    void setReadNorm(bool value);
    
    bool getAutoload() const;
    void setAutoload(bool value);
    
private:
    QString fname_;
    QString baseFolder;
    QString prevBase;
    bool readNorm;
    bool autoload;
};

#endif // SETTINGSPROCESSOR_H
