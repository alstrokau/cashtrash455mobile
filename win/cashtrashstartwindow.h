#ifndef CASHTRASHSTARTWINDOW_H
#define CASHTRASHSTARTWINDOW_H

#include <QMainWindow>
#include "win/cashtrash455m.h"
#include "tools/settingsProcessor/settingsprocessor.h"
#include "win/settingswindow.h"

namespace Ui {
class cashtrashStartWindow;
}

class cashtrashStartWindow : public QMainWindow
{
    Q_OBJECT

static QString settingsFileName_;
    
public:
    explicit cashtrashStartWindow(QWidget *parent = 0);
    ~cashtrashStartWindow();

private slots:
    void actClose();
    void updateBaseFolder(QString baseFolder);
    void updateAutoload(bool checked);
    
    void on_pbClose_clicked();
    void on_pbOpen_clicked();
    void on_pbNew_clicked();
    void on_pbPrev_clicked();
    void on_pbSettings_clicked();
    
private:
    Ui::cashtrashStartWindow *ui;
    
    settingsProcessor settingsProcessor_;
    CashTrash455m* baseLoadWin_;
    SettingsWindow* settingsWindow_;
    
    QString baseFolder_;
    QString prevBaseFName_;
    
    void loadSettings();
    void loadPrevBase();
    
    
};

#endif // CASHTRASHSTARTWINDOW_H
